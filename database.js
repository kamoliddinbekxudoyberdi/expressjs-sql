import mysql from "mysql2";
import dotenv from "dotenv";
dotenv.config();
import fs from "fs";

const pool = mysql.createPool({
  host: process.env.MYSQL_HOST,
  user: process.env.MYSQL_USER,
  password: process.env.MYSQL_PASSWORD,
  database: process.env.MYSQL_DATABASE,
})
.promise();

const initialQuery = fs.readFileSync("./schema.sql").toString();
await pool.query(initialQuery);

export const getAllNotes = async () => {
  const [result] = await pool.query('SELECT * FROM Notes');
  return result;
}

export const getOneNote = async (id) => {
  const [result] = await pool.query("SELECT * FROM Notes WHERE NotesID = ?", [id]);
  return result[0];
}

export const createNote = async (payload) => {
  const [newNote] = await pool.query("INSERT INTO Notes (Title, Content) VALUES (?, ?)", [payload.title, payload.content]);
  return newNote;
}

export const updateNote = async (id, payload) => {
  const searchedNote = await getOneNote(id);
  if(!searchedNote) {
    return;
   } else {
    const [updatedNote] = await pool.query("UPDATE Notes SET title=?, content=? WHERE NotesID = ?", [payload?.title || searchedNote.Title, payload?.content || searchedNote.Content, id]);
    return await getOneNote(id)
  }
}

export const deleteNote = async (id, res) => {
  const searchedNote = await getOneNote(id);
  if(!searchedNote) {
    res.status(404).send({message: `Note is not found with id: ${id}`});
  } else {
    let [deletedNote] = await pool.query("DELETE FROM Notes WHERE NotesID = ?", [id]);
    res.status(200).send({message: "Successfully deleted", id})
  }
}