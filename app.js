import express from "express";
import { createNote, deleteNote, getAllNotes, getOneNote, updateNote } from "./database.js";
const app = express();

app.use(express.json());

app.get("/notes/:id", async (req, res) => {
  const id = req.params.id;
  const note = await getOneNote(id);
  if(note) {
    res.status(200).send(note);
  } else {
    res.status(404).send({message: `Note is not found width id: ${id}`})
  }
})

app.get('/notes', async (req, res) => {
  const notes = await getAllNotes();
  res.status(200).send(notes);
})

app.post("/notes", async (req, res) => {
  const {title, content} = req.body;
  if(title && content) {
    const newNote = await createNote({title, content});
    const addedNote = await getOneNote(newNote.insertId);
    console.log(newNote)
    res.status(201).send(addedNote);
  } else {
    res.status(400).send({message: "All params are required"});
  }
});

app.put("/notes/:id", async (req, res) => {
  const id = req.params.id;
  const updatedNote = await updateNote(id, req.body);
  if(!updatedNote) {
    res.status(404).send({message: `Note not found with id: ${id}`});
  } else {
    res.send(updatedNote);
  }
});

app.delete("/notes/:id", async (req, res) => {
  const id = req.params.id;
  const deletedNote = await deleteNote(id, res);
})

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('Something went wrong!')
});

const PORT = 3000;
app.listen(PORT, () => {
  console.log(`Server is running on port: ${PORT}`)
});